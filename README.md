# Open Tibia Classic - JSONRPC

TODO update this file

For dispatch, check varargs against component type, that means you cant pass array[] for a varargs parameter like you can in java.

EX.

public void foo(final String s, final String ... extra) {

}

VALID JAVA:
foo("Hello, world!", new String[]{"Good-bye", "world!"})
foo("Hello, world!", "Good-bye", "world!")

INVALID JSON-RPC REQUEST
{
    "method": "foo",
    "params": ["Hello, world!", ["Good-bye", "world!"]]
}

VALID JSON-RPC REQUEST
{
    "method": "foo",
    "params": ["Hello, world!", "Good-bye", "world!"]
}


be careful of method hiding
        //public int unambiguous(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i)
            //public void unambiguous(final @JSONRPCParameter("i") int i, final @JSONRPCParameter("s") String s)

void unambiguous cant be called by named parameters, because int unambiguous is always more specific! 


Note: Dispatcher::dispatch("[],") and Dispatcher::dispatch("[{},]") are both successfully dispatched as batch (org.json ignores trailing commas in parsing)

org.json.JSONObject::get() returns JSONObject.NULL if the key is null in the response object

## Running Tests

TODO talk about logging.properties