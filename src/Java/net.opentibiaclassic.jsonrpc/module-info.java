module net.opentibiaclassic.jsonrpc {
    requires org.json;
    requires transitive net.opentibiaclassic;
    exports net.opentibiaclassic.jsonrpc;
    exports net.opentibiaclassic.jsonrpc.errors;
}