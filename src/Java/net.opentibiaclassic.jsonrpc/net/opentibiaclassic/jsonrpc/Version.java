package net.opentibiaclassic.jsonrpc;

public class Version {
    private static boolean isBlankString(String string) {
        return string == null || string.trim().isEmpty();
    }

    public static final int MAJOR=1;
    public static final int MINOR=1;
    public static final int PATCH=0;
    public static final String PRE_RELEASE="alpha";
    public static final String SEMVER=String.format(
        "%d.%d.%d%s",
        MAJOR,
        MINOR,
        PATCH,
        isBlankString(PRE_RELEASE) ? "" : String.format("-%s", PRE_RELEASE)
    );
}