package net.opentibiaclassic.jsonrpc;

import java.lang.reflect.Method;
import java.util.List;

public class AmbiguousMethodException extends Exception {
    public AmbiguousMethodException(final List<Method> overloads) {
        super(String.format("error: reference to %s is ambiguous.\n methods: %s match.", overloads.get(0).getName(), Util.methodSignatures(overloads)));
    }
}