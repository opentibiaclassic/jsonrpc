package net.opentibiaclassic.jsonrpc.errors;

import org.json.JSONException;

public class ServerError extends JSONRPCException {
    public ServerError(final Exception ex) {
        super(ErrorCodes.SERVER_ERROR, ex);
    }
}