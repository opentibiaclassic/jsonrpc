package net.opentibiaclassic.jsonrpc.errors;

import org.json.JSONObject;
import org.json.JSONException;

public abstract class JSONRPCException extends java.lang.Exception {

    private final Object data;
    private final int code;

    public JSONRPCException(final int code, final Throwable ex, final Object data) {
        this(code, String.format("%s: %s", ex.getClass().getSimpleName(), ex.getMessage()), data);
    }

    public JSONRPCException(final int code, final Throwable ex) {
        this(code, ex, null);
    }

    public JSONRPCException(final int code, final String message) {
        this(code, message, null);
    }

    public JSONRPCException(final int code, final String message, final Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }

    public final JSONObject getError() {
        final JSONObject error = new JSONObject();
        error.put("code", this.code)
            .put("message", this.getMessage());

        try {
            error.put("data", this.data);
        } catch (JSONException ex) {
            error.put("data", ex.getMessage());
        }

        return error;
    }
}