package net.opentibiaclassic.jsonrpc.errors;

import org.json.JSONException;

public class InvalidRequestError extends JSONRPCException {

    private static final String MESSAGE = "Invalid Request";

    public InvalidRequestError(final Object data) {
        super(ErrorCodes.INVALID_REQUEST, MESSAGE, data);
    }
}