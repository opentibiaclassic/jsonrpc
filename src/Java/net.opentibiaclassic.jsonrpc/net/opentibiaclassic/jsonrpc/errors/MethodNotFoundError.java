package net.opentibiaclassic.jsonrpc.errors;

import net.opentibiaclassic.jsonrpc.Request;

import org.json.JSONException;

public class MethodNotFoundError extends JSONRPCException {
    private static final String MESSAGE = "Method not found";

    public MethodNotFoundError(final Object data) {
        super(ErrorCodes.METHOD_NOT_FOUND, MESSAGE, data);
    }

    public MethodNotFoundError(final Request request) {
        this(request.getSignature());
    }
}