package net.opentibiaclassic.jsonrpc.errors;

public class ErrorCodes {
    public final static int PARSE_ERROR = -32700;
    public final static int INVALID_REQUEST = -32600;
    public final static int METHOD_NOT_FOUND = -32601;
    public final static int INVALID_PARAMETERS = -32602;
    public final static int INTERNAL_ERROR = -32603;
    public final static int SERVER_ERROR = -32000;
}