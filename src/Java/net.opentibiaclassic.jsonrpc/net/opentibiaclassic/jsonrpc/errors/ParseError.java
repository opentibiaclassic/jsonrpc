package net.opentibiaclassic.jsonrpc.errors;

import org.json.JSONException;

public class ParseError extends JSONRPCException {
    public ParseError(final JSONException ex, final Object data) {
        super(ErrorCodes.PARSE_ERROR, ex, data);
    }
}