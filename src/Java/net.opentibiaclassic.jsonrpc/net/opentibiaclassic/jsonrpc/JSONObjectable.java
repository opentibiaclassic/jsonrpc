package net.opentibiaclassic.jsonrpc;

import org.json.JSONObject;

public abstract class JSONObjectable implements JSONable {
    abstract public JSONObject toJSONObject();

    @Override
    public String toJSON() {
        return this.toJSONObject().toString();
    }
}