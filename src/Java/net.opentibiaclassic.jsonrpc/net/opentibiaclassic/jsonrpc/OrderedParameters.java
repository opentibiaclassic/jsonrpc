package net.opentibiaclassic.jsonrpc;

import java.util.List;
import java.util.ArrayList;

import org.json.JSONArray;

public class OrderedParameters<T> extends ArrayList<T> {
    public static <K> OrderedParameters<K> from(List<K> l) {
        if (null == l) {
            return new OrderedParameters();
        }

        return new OrderedParameters(l);
    }

    public OrderedParameters(List<T> l) {
        super(l);
    }

    public OrderedParameters() {
        super();
    }

    public JSONArray toJSONArray() {
        return new JSONArray((Iterable)this);
    }
}