package net.opentibiaclassic.jsonrpc;

import net.opentibiaclassic.jsonrpc.errors.JSONRPCException;
import net.opentibiaclassic.jsonrpc.errors.MethodNotFoundError;

import org.json.JSONObject;

public class Error extends Response {
    
    private final JSONRPCException exception;

    public Error(final JSONRPCException exception, final Object id) {
        super(id);
        this.exception = exception;
    }

    public Error(final JSONRPCException exception) {
        this(exception, null);
    }

    public JSONObject toJSONObject() {
        return super.toJSONObject()
            .put("error", this.exception.getError());
    }
}