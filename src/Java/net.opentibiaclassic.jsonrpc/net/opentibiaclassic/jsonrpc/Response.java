package net.opentibiaclassic.jsonrpc;

import org.json.JSONObject;

import net.opentibiaclassic.jsonrpc.errors.JSONRPCException;

public abstract class Response extends JSONObjectable {

    protected final String jsonrpc = Const.JSONRPC_VERSION;
    private final Object id;

    public Response(final Object id) {
        this.id = id;
    }

    public Object getId() {
        return this.id;
    }

    @Override
    public JSONObject toJSONObject() {
        return new JSONObject()
            .put("jsonrpc", this.jsonrpc)
            .put("id", this.id == null ? JSONObject.NULL : this.id);
    }

    @Override
    public final String toString() {
        return this.toJSON();
    }
}