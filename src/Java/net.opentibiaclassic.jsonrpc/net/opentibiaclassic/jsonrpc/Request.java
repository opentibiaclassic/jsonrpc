package net.opentibiaclassic.jsonrpc;

import net.opentibiaclassic.jsonrpc.errors.InvalidRequestError;
import net.opentibiaclassic.jsonrpc.Util;

import org.json.JSONObject;
import org.json.JSONException;

public class Request extends JSONObjectable {
    
    private final String jsonrpc;
    private final Object id;
    private final String method;

    public Request(final String methodName) {
        this(null, methodName);
    }

    public Request(final Object id, final String methodName) {
        this.id = id;
        this.method = methodName;
        this.jsonrpc = Const.JSONRPC_VERSION;
    }

    private static final boolean isValidIdType(final Object id) {
        return JSONObject.NULL == id || null == id || id instanceof String || id instanceof Number;
    }

    private static final boolean isInvalidIdType(final Object id) {
        return !isValidIdType(id);
    }

    private static final boolean isValidJSONRPC(final String jsonrpc) {
        return Const.JSONRPC_VERSION.equals(jsonrpc);
    }

    private static final boolean isInvalidJSONRPC(final String jsonrpc) {
        return !isValidJSONRPC(jsonrpc);
    }

    public Request(final JSONObject remoteCall) throws InvalidRequestError {
        try {
            final Object remoteCallId = remoteCall.opt("id");

            if (null == remoteCallId && remoteCall.has("id")) {
                this.id = JSONObject.NULL;
            } else {
                this.id = remoteCallId;
            }

            if (isInvalidIdType(this.id)) {
                throw new Exception();
            }

            this.jsonrpc = remoteCall.getString("jsonrpc");

            if (isInvalidJSONRPC(this.jsonrpc)) {
                throw new Exception();
            }

            this.method = remoteCall.getString("method");

            if (this.method.isEmpty()) {
                throw new Exception();
            }

        } catch (final Exception ex) {
            throw new InvalidRequestError(remoteCall);
        }
    }

    public String getSignature() {
        return Util.methodSignature(this.getMethod());
    }

    public Object getId() {
        return this.id;
    }

    public boolean isNotification() {
        return null == this.getId();
    }

    public String getMethod() {
        return this.method;
    }

    @Override
    public final String toString() {
        return this.toJSON();
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("jsonrpc", this.jsonrpc)
            .put("method", this.method);

        if (null != this.id) {
            object.put("id", this.id);
        }

        return object;
    }
}