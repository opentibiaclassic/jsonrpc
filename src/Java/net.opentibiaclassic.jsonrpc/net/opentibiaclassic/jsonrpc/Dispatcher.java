package net.opentibiaclassic.jsonrpc;

// TODO stop importing unused classes

import java.util.Arrays;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;
import java.util.stream.Collectors;
import java.util.UUID;
import java.util.Collections;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.annotation.Annotation;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.jsonrpc.errors.JSONRPCException;
import net.opentibiaclassic.jsonrpc.errors.ParseError;
import net.opentibiaclassic.jsonrpc.errors.ServerError;
import net.opentibiaclassic.jsonrpc.errors.MethodNotFoundError;
import net.opentibiaclassic.jsonrpc.errors.InvalidRequestError;

public abstract class Dispatcher {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Dispatcher.class.getCanonicalName());

    public class RequestBuilder {

        private final Dispatcher dispatcher;
        private final List<Request> batch;

        public RequestBuilder(final Dispatcher dispatcher) {
            this.dispatcher = dispatcher;
            batch = new LinkedList<Request>();
        }

        private RequestBuilder addRequest(final Object id, final String methodName) throws AmbiguousMethodException, NoSuchMethodException {
            final Method method = this.dispatcher.getMethod(methodName);

            this.batch.add(new Request(id, methodName));

            return this;
        }

        private RequestBuilder addRequest(final Object id, final String methodName, Object ... args) throws AmbiguousMethodException, NoSuchMethodException {
            final OrderedParameters parameters = OrderedParameters.from(Arrays.asList(args));
            final Method method = this.dispatcher.getMethod(methodName, parameters);

            this.batch.add(new OrderedParametersRequest(id, methodName, parameters));

            return this;
        }

        public RequestBuilder addRequest(final Number id, final String methodName) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)id, methodName);
        }

        public RequestBuilder addRequest(final Number id, final String methodName, Object ... args) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)id, methodName, args);
        }

        public RequestBuilder addRequest(final String id, final String methodName) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)id, methodName);
        }

        public RequestBuilder addRequest(final String id, final String methodName, Object ... args) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)id, methodName, args);
        }

        public RequestBuilder addNotification(final String methodName) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)null, methodName);
        }

        public RequestBuilder addNotification(final String methodName, Object ... args) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)null, methodName, args);
        }

        private RequestBuilder addRequest(final Object id, final String methodName, Map args) throws AmbiguousMethodException, NoSuchMethodException {
            final NamedParameters parameters = NamedParameters.from(args);
            final Method method = this.dispatcher.getMethod(methodName, parameters);

            this.batch.add(new NamedParametersRequest(id, methodName, parameters));

            return this;
        }

        public RequestBuilder addRequest(final Number id, final String methodName, Map args) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)id, methodName, args);
        }

        public RequestBuilder addRequest(final String id, final String methodName, Map args) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)id, methodName, args);
        }

        public RequestBuilder addNotification(final String methodName, Map args) throws AmbiguousMethodException, NoSuchMethodException {
            return this.addRequest((Object)null, methodName, args);
        }

        public List<Request> getRequests() {
            return Collections.unmodifiableList(this.batch);
        }

        public void reset() {
            this.batch.clear();
        }

        public String toString() {
            return this.toJSON();
        }

        public String toJSON() {
            switch (batch.size()) {
                case 0:
                    return null;
                case 1:
                    return ((Request)batch.get(0)).toJSON();
                default:
                    return (new JSONArray(
                        (Iterable)batch.stream()
                            .map(r -> r.toJSONObject())
                            .collect(Collectors.toList())
                    )).toString();
            }

        }
    }

    // TODO java doc everything
    private JSONRPCParameter getParameterAnnotation(Parameter p) {
        return (JSONRPCParameter)p.getAnnotation(JSONRPCParameter.class);
    }

    private String getParameterName(Parameter p) {
        return getParameterAnnotation(p).value();
    }

    private OrderedParameters getOrderedParameters(final Method method, final NamedParameters parameters) {
        return OrderedParameters.from(
            Arrays.stream(method.getParameters())
                .limit(parameters.size())
                .map(p -> parameters.get(this.getParameterName(p)))
                .collect(Collectors.toList())
        );
    }

    private boolean hasParameter(final NamedParameters parameters, final Parameter parameter) {
        final JSONRPCParameter annotation = getParameterAnnotation(parameter);

        if (null == annotation || !parameters.containsKey(annotation.value())) {
            return false;
        }

        return true;
    }

    private boolean notHasParameter(final NamedParameters parameters, final Parameter parameter) {
        return !hasParameter(parameters, parameter);
    }

    private boolean isNamedParameterMatch(final Method method, final NamedParameters parameters) {
        logger.entering(method, parameters);

        final int minimumParameterCount = Util.minimumParameterCount(method);

        final boolean isExactParameterCount = method.getParameters().length == parameters.size();
        final boolean notIsExactParameterCount = !isExactParameterCount;
        final boolean notIsMinimumParameterCount = minimumParameterCount != parameters.size();

        if (notIsExactParameterCount && notIsMinimumParameterCount) {
            return false;
        }

        final Parameter[] methodParameters = method.getParameters();

        for (int i=0;i<minimumParameterCount; i++) {
            if (notHasParameter(parameters, methodParameters[i])) {
                return false;
            }
        }

        if (method.isVarArgs() && isExactParameterCount && notHasParameter(parameters, Util.Arrays.last(methodParameters))) {
            return false;
        }

        return true;
    }

    private List<Method> getDeclaredPublicMethods(final String methodName) {
        logger.entering(methodName);

        return Arrays.stream(this.getClass().getDeclaredMethods())
            .filter(m -> m.getName().equalsIgnoreCase(methodName))
            .filter(m -> Modifier.isPublic(m.getModifiers()))
            .collect(Collectors.toList());
    }

    private boolean isParameterTypeMatch(final Method method, OrderedParameters parameters) {
        logger.entering(method, parameters);
        final Class[] types = method.getParameterTypes();

        int i = 0;
        for (;i<Util.minimumParameterCount(method); i++) {
            final Object parameter = parameters.get(i);

            if (!Util.isAssignable(parameter, types[i])) {
                return false;
            }
        }

        if (method.isVarArgs()) {
            final boolean isLastParameter = i == parameters.size() - 1;
            if (isLastParameter) {
                final Class variadicArrayClass = types[types.length - 1];
                final Object lastParameter = parameters.get(i);

                if (Util.isAssignable(lastParameter, variadicArrayClass)) {
                    // The last parameter is an Array assignable to our variadic array
                    return true;
                }
            }

            final Class variadicClass = types[types.length - 1].getComponentType();
            for (; i<parameters.size(); i++) {
                final Object parameter = parameters.get(i);

                if (!Util.isAssignable(parameter, variadicClass)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isParameterCountMatch(final Method method, OrderedParameters parameters) {
        logger.entering(method, parameters);

        if (method.isVarArgs()) {
            return Util.minimumParameterCount(method) <= parameters.size();
        }

        return method.getParameterCount() == parameters.size();
    }

    private Method getMethod(final String methodName, final NamedParameters parameters) throws AmbiguousMethodException, NoSuchMethodException {
        logger.entering(methodName, parameters);
        final List<Method> candidateMethods = this.getDeclaredPublicMethods(methodName).stream()
            .filter(m -> this.isNamedParameterMatch(m, parameters))
            .filter(m -> this.isParameterTypeMatch(m, this.getOrderedParameters(m, parameters)))
            .collect(Collectors.toList());

        return Util.selectMostSpecificMethod(candidateMethods);
    }

    private Method getMethod(final String methodName, final OrderedParameters parameters) throws AmbiguousMethodException, NoSuchMethodException {
        logger.entering(methodName, parameters);
        final List<Method> candidateMethods = this.getDeclaredPublicMethods(methodName).stream()
            .filter(m -> this.isParameterCountMatch(m, parameters))
            .filter(m -> this.isParameterTypeMatch(m, parameters))
            .collect(Collectors.toList());

        return Util.selectMostSpecificMethod(candidateMethods);
    }

    private Method getMethod(final String methodName) throws AmbiguousMethodException, NoSuchMethodException {
        return this.getMethod(methodName, new OrderedParameters());
    }

    private Response invoke(final Object id, final Method method, final Object ... parameters) {
        logger.entering(id, method, parameters);
        try {
            final Object result = method.invoke(this, parameters);

            if (Util.isVoid(method)) {
                // WARNING! Breaks JSON-RPC spec of ALWAYS returning a 'result' for a Request
                return new Success(id);
            }

            return new Result(id, result);

        } catch(final Exception ex) {
            logger.warning(ex);
            return new Error(new ServerError(ex), id);
        }
    }

    private Response invoke(final NamedParametersRequest request) throws AmbiguousMethodException, NoSuchMethodException {
        logger.entering(request);
        final Object id = request.getId();
        final NamedParameters parameters = request.getParameters();
        final Method method = getMethod(request.getMethod(), parameters);

        return this.invoke(id, method, this.getOrderedParameters(method, parameters).toArray());
    }

    private Response invoke(final OrderedParametersRequest request) throws AmbiguousMethodException, NoSuchMethodException {
        logger.entering(request);
        final Object id = request.getId();
        final OrderedParameters parameters = request.getParameters();
        final Method method = getMethod(request.getMethod(), parameters);

        return this.invoke(id, method, parameters.toArray());
    }

    private Response invoke(final Request request) throws AmbiguousMethodException, NoSuchMethodException {
        logger.entering(request);
        final Object id = request.getId();
        final Method method = getMethod(request.getMethod());

        return this.invoke(id, method);
    }

    private Response dispatchRequest(final Request request) {
        logger.entering(request);
        Response response;
        final Object id = request.getId();

        try {
            if (request instanceof OrderedParametersRequest) {
                response = invoke((OrderedParametersRequest) request);
            } else if (request instanceof NamedParametersRequest) {
                response = invoke((NamedParametersRequest) request);
            } else {
                response = invoke(request);
            }
        } catch(final NoSuchMethodException ex) {
            logger.warning(ex);
            response = new Error(new MethodNotFoundError(request), id);
        } catch(final Exception ex) {
            logger.warning(ex);
            response = new Error(new ServerError(ex), id);
        }

        if (request.isNotification()) {
            return null;
        }

        return response;
    }

    private JSONObject dispatchCall(final JSONObject remoteCall) {
        logger.entering(remoteCall);
        Response response;

        final Object parameters = remoteCall.opt("params");

        try {
            if (null == parameters) {
                response = dispatchRequest(new Request(remoteCall));
            } else if (parameters instanceof JSONObject) {
                response = dispatchRequest(new NamedParametersRequest(remoteCall));
            } else {
                response = dispatchRequest(new OrderedParametersRequest(remoteCall));
            }
        } catch(final InvalidRequestError ex) {
            logger.warning(ex);
            response = new Error(ex);
        }

        if (null == response) {
            return null;
        }

        return response.toJSONObject();
    }

    private String dispatchCall(final String json) throws ParseError {
        logger.entering(json);
        try {
            final JSONObject result = dispatchCall(new JSONObject(json));

            if (null == result) {
                return null;
            }

            logger.exiting(result.toString());
            return result.toString();

        } catch (final JSONException ex) {
            logger.throwing(ex);
            throw new ParseError(ex, json);
        }
    }

    private JSONArray dispatchBatch(final JSONArray remoteCalls) {
        logger.entering(remoteCalls);
        final JSONArray response = new JSONArray();

        for(Object call : remoteCalls) {
            try {
                JSONObject result = this.dispatchCall((JSONObject)call);

                if (null != result) {
                    response.put(result);
                }

            } catch(final ClassCastException ex) {
                logger.warning(ex);
                response.put(new Error(new InvalidRequestError(call)));
            }
        }

        return response;
    }

    private String dispatchBatch(final String json) throws ParseError {
        logger.entering(json);
        try {
            final JSONArray results = dispatchBatch(new JSONArray(json));

            if (results.isEmpty()) {
                return null;
            }

            logger.exiting(results.toString());
            return results.toString();
        } catch (final JSONException ex) {
            logger.throwing(ex);
            throw new ParseError(ex, json);
        }
    }

    public final String dispatch(final String json) {
        logger.entering(json);

        try {
            final String trimmedJSON = json.trim();

            if (trimmedJSON.isEmpty()) {
                return null;
            }

            if (Util.JSON.isJSONifiedArray(trimmedJSON)) {
                return dispatchBatch(trimmedJSON);
            }

            return dispatchCall(trimmedJSON);

        } catch (final ParseError ex) {
            logger.warning(ex);
            return new Error(ex).toJSON();
        } catch (final Exception ex) {
            logger.warning(ex);
            return new Error(new ServerError(ex)).toJSON();
        }
    }

    public final RequestBuilder getRequestBuilder() {
        return new RequestBuilder(this);
    }
}