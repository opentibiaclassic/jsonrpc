package net.opentibiaclassic.jsonrpc;

import java.lang.reflect.Method;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;
import java.util.stream.Collectors;

public class Util {
    public static class Arrays {
        public static <T> T last(T[] array) {
            if (0 == array.length) {
                return null;
            }

            return array[array.length - 1];
        }
    }

    public static class JSON {
        public static String typeName(final Object parameter) {
            if (null == parameter) {
                return "null";
            }

            if (parameter instanceof String) {
                return "string";
            }

            if (parameter instanceof Number) {
                return "number";
            }

            if (parameter instanceof HashMap) {
                return "object";
            }

            if (parameter instanceof ArrayList) {
                return "array";
            }

            return parameter.toString();
        }

        public static boolean isJSONifiedArray(final String trimmedJSON) {
            return '[' == trimmedJSON.charAt(0);
        }
    }

    private static String parameterSignature(final String ... types) {
        return String.format("(%s)",String.join(",", types));
    }

    private static String parameterSignature(final Object ... parameters) {
        return parameterSignature(java.util.Arrays.stream(parameters)
            .map(p -> JSON.typeName(p))
            .toArray(String[]::new)
        );
    }

    private static String parameterSignature(final Class ... parameterTypes) {
        return parameterSignature(java.util.Arrays.stream(parameterTypes)
            .map(t -> t.getSimpleName())
            .toArray(String[]::new)
        );
    }

    private static String methodSignature(final String methodName, final String parameterSignature) {
        return String.format(
            "%s%s",
            methodName,
            parameterSignature
        );
    }

    public static String methodSignature(final String methodName, final Object ... parameters) {
        return methodSignature(methodName, parameterSignature(parameters));
    }

    private static String methodSignature(final String methodName, final Class ... parameterTypes) {
        return methodSignature(methodName, parameterSignature(parameterTypes));
    }

    public static String methodSignature(final Method method) {
        return methodSignature(method.getName(), method.getParameterTypes());
    }

    public static String methodSignatures(final List<Method> methods) {
        return String.join(", ", methods.stream()
            .map(m -> methodSignature(m))
            .toArray(String[]::new)
        );
    }

    public static boolean isVoid(final Method method) {
        return method.getReturnType().equals(Void.TYPE);
    }

    public static int score(final Class c) {
        if (null == c) {
            return 0;
        }

        return 1 + c.getInterfaces().length + score(c.getSuperclass());
    }

    public static int score(final Method method) {

        final int returnScore;
        if (isVoid(method)) {
            returnScore = 0;
        } else {
            returnScore = score(method.getReturnType());
        }

        return java.util.Arrays.stream(method.getParameterTypes())
            .map(c -> score(c))
            .reduce(0, Integer::sum) + returnScore;
    }

    private static final Map<Class, Class> primitiveToWrapper = Map.of(
        boolean.class, Boolean.class,
        byte.class, Byte.class,
        char.class, Character.class,
        float.class, Float.class,
        int.class, Integer.class,
        long.class, Long.class,
        short.class, Short.class,
        double.class, Double.class
    );

    public static Class unbox(final Class boxed) {
        if (primitiveToWrapper.containsKey(boxed)) {
            return primitiveToWrapper.get(boxed);
        }

        return boxed;
    }

    private static final Map<Class, List<Class>> primitiveToWide = Map.of(
        byte.class, List.of(short.class, int.class, long.class, float.class, double.class),
        short.class, List.of(int.class, long.class, float.class, double.class),
        char.class, List.of(int.class, long.class, float.class, double.class),
        int.class, List.of(long.class, float.class, double.class),
        long.class, List.of(float.class, double.class),
        float.class, List.of(double.class)
    );

    public static boolean canWiden(final Class from, final Class to) {
        // The Java® Language Specification Java SE 18 Edition
        // 5.1.2. Widening Primitive Conversion 

        return primitiveToWide.containsKey(from) && primitiveToWide.get(from).contains(to);
    }

    public static boolean isPrimitiveChar(final Class c) {
        return char.class == c;
    }

    public static boolean isPrimitiveByte(final Class c) {
        return byte.class == c;
    }

    public static boolean canConvert(final Class from, final Class to) {
        if (isPrimitiveByte(from) && isPrimitiveChar(to)) {
            // The Java® Language Specification Java SE 18 Edition
            // 5.1.4. Widening and Narrowing Primitive Conversion
            return true;
        }

        return canWiden(from, to);
    }

    public static boolean isAssignable(final Class from, final Class to) {
        if (unbox(to).isAssignableFrom(unbox(from))) {
            return true;
        }

        return canConvert(from, to);
    }

    public static boolean isAssignable(final Object from, final Class to) {
        if (null == from) {
            return !to.isPrimitive();
        }

        return isAssignable(from.getClass(), to);
    }

    public static int minimumParameterCount(final Method method) {
        final int count = method.getParameterCount();

        if (method.isVarArgs()) {
            return count - 1;
        }

        return count;
    }

    public static Method selectMostSpecificMethod(final List<Method> methods) throws AmbiguousMethodException, NoSuchMethodException {
        if (0 == methods.size()) {
            throw new NoSuchMethodException();
        }

        final List<Entry<Method,Integer>> scoredMethods = methods.stream()
            .map(m -> new SimpleEntry<Method, Integer>(m, score(m)))
            .collect(Collectors.toList());

        final int maxSpecificity = scoredMethods.stream()
            .map(e -> e.getValue())
            .mapToInt(Integer::intValue)
            .max()
            .getAsInt();

        final List<Method> mostSpecificMethods = scoredMethods.stream()
            .filter(e -> e.getValue() == maxSpecificity)
            .map(e -> e.getKey())
            .collect(Collectors.toList());

        if (1 < mostSpecificMethods.size()) {
            throw new AmbiguousMethodException(mostSpecificMethods);
        }

        return mostSpecificMethods.get(0);
    }
}