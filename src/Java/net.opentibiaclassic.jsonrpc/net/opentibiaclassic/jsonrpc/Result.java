package net.opentibiaclassic.jsonrpc;

import net.opentibiaclassic.jsonrpc.errors.JSONRPCException;

import org.json.JSONObject;

public class Result extends Success {
    private final Object result;

    public Result(final Object id, final Object result) {
        super(id);
        this.result = result;
    }

    public JSONObject toJSONObject() {
        return super.toJSONObject()
            .put("result", null == this.result ? JSONObject.NULL : this.result);
    }
}