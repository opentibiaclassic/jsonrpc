package net.opentibiaclassic.jsonrpc;

public interface JSONable {
    public String toJSON();
}