package net.opentibiaclassic.jsonrpc;

import net.opentibiaclassic.jsonrpc.errors.JSONRPCException;

import org.json.JSONObject;

public class Success extends Response {
    
    public Success(final Object id) {
        super(id);
    }
}