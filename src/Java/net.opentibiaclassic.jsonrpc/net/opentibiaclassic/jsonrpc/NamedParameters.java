package net.opentibiaclassic.jsonrpc;

import java.util.Map;
import java.util.HashMap;

import org.json.JSONObject;

// TODO only take <String, T>

public class NamedParameters<T,K> extends HashMap<T,K> {
    public static <K, V> NamedParameters<K, V> from(Map<K, V> m) {
        if (null == m) {
            return new NamedParameters();
        }

        return new NamedParameters<K, V>(m);
    }

    public NamedParameters(Map<T,K> m) {
        super(m);
    }

    public NamedParameters() {
        super();
    }

    public JSONObject toJSONObject() {
        return new JSONObject((Map)this);
    }
}