package net.opentibiaclassic.jsonrpc;

import net.opentibiaclassic.jsonrpc.errors.InvalidRequestError;

import org.json.JSONObject;
import org.json.JSONException;

public class NamedParametersRequest extends Request {
    
    private final NamedParameters parameters;

    public NamedParametersRequest(final Object id, final String methodName, final NamedParameters parameters) {
        super(id, methodName);

        if (null == parameters) {
            this.parameters = new NamedParameters();
        } else {
            this.parameters = parameters;
        }
    }

    public NamedParametersRequest(final String methodName, final NamedParameters parameters) {
        this(null, methodName, parameters);
    }

    public NamedParametersRequest(final JSONObject remoteCall) throws InvalidRequestError {
            super(remoteCall);

        try {
            this.parameters = NamedParameters.from(remoteCall.getJSONObject("params").toMap());
        } catch (final Exception ex) {
            throw new InvalidRequestError(remoteCall);
        }
    }

    public String getSignature() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public NamedParameters getParameters() {
        return this.parameters;
    }

    public JSONObject toJSONObject() {
        JSONObject object = super.toJSONObject();
        object.put("params", this.getParameters().toJSONObject());
        return object;
    }
}