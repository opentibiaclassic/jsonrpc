package net.opentibiaclassic.jsonrpc;

import net.opentibiaclassic.jsonrpc.errors.InvalidRequestError;
import net.opentibiaclassic.jsonrpc.Util;

import org.json.JSONObject;
import org.json.JSONException;

public class OrderedParametersRequest extends Request {
    
    private final OrderedParameters parameters;

    public OrderedParametersRequest(final String methodName, final OrderedParameters parameters) {
        this(null, methodName, parameters);
    }

    public OrderedParametersRequest(final Object id, final String methodName, final OrderedParameters parameters) {
        super(id, methodName);

        if (null == parameters) {
            this.parameters = new OrderedParameters();
        } else {
            this.parameters = parameters;
        }
    }

    public OrderedParametersRequest(final JSONObject remoteCall) throws InvalidRequestError {
        super(remoteCall);

        try {
            this.parameters = OrderedParameters.from(remoteCall.getJSONArray("params").toList());
        } catch (final Exception ex) {
            throw new InvalidRequestError(remoteCall);
        }
    }

    public String getSignature() {
        return Util.methodSignature(this.getMethod(), this.getParameters().toArray());
    }

    public OrderedParameters getParameters() {
        return this.parameters;
    }

    public int getParameterCount() {
        return this.getParameters().size();
    }

    public JSONObject toJSONObject() {
        JSONObject object = super.toJSONObject();
        object.put("params", this.getParameters().toJSONArray());
        return object;
    }
}