#!/bin/bash
set -x
log_file=test/Java/test.log

test () {
    java -Djava.util.logging.config.file=logging.properties --module-path build/Java:lib/Java --add-modules net.opentibiaclassic.jsonrpc org.junit.platform.console.ConsoleLauncher --class-path=build/Java --select-class=$1 --reports-dir=test/ --fail-if-no-tests --disable-banner --details=summary --details-theme=unicode 1>>$log_file 2>&1
}

# clear file
> $log_file

# Run tests in order, since they rely on each other (i.e. to RequestBuilderTests depend on Requests)
ordered=( 
    test.Java.net.opentibiaclassic.jsonrpc.ResponseTests
    test.Java.net.opentibiaclassic.jsonrpc.RequestTests
    test.Java.net.opentibiaclassic.jsonrpc.RequestBuilderTests
    test.Java.net.opentibiaclassic.jsonrpc.DispatcherTests
)

for class in "${ordered[@]}"
do
    printf '%s\n' $class '-----------------------------------------------------' >> $log_file
    test $class
done