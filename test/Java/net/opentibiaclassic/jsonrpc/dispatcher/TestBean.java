package test.Java.net.opentibiaclassic.jsonrpc.dispatcher;

    public class TestBean {
        private final int x;
        private final String y;

        public TestBean(final int x, final String y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return this.x;
        }

        public String getY() {
            return this.y;
        }
    }