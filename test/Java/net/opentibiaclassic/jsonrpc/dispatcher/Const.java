package test.Java.net.opentibiaclassic.jsonrpc.dispatcher;

public class Const {
    public static final String[] VALID_STRING_IDS = new String[] {
        (String)null,
        "uuid",
        ""
    };

    public static final Number[] VALID_NUMBER_IDS = new Number[] {
        (Number)null,
        1,
        1.5,
        0,
        0.0
    };
}