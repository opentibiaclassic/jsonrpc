package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.function.Executable;

import java.util.stream.Stream;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;

import net.opentibiaclassic.jsonrpc.Dispatcher;
import net.opentibiaclassic.jsonrpc.Request;
import net.opentibiaclassic.jsonrpc.NamedParameters;
import net.opentibiaclassic.jsonrpc.NamedParametersRequest;
import net.opentibiaclassic.jsonrpc.OrderedParameters;
import net.opentibiaclassic.jsonrpc.OrderedParametersRequest;

public class ValidRequestProvider extends RequestArgumentsProvider {
    public static final Map<String, List<OrderedParameters>> ORDERED_PARAMETERS_BY_METHOD_NAME = Map.of(
        // public void ambiguous(final String s, final Object o)
        "ambiguous", List.of(
            OrderedParameters.from(List.of(new String(), new Object())),
            OrderedParameters.from(Arrays.asList((String)null, new Object())),
            OrderedParameters.from(List.of(new String(), 0)),

            // public void ambiguous(final Object o, final String s)
            OrderedParameters.from(List.of(new Object(), new String())),
            OrderedParameters.from(Arrays.asList(new Object(), (String)null)),
            OrderedParameters.from(List.of(0, new String()))
        ),

        // public void noReturnNoParameters()
        "noReturnNoParameters", List.of(new OrderedParameters()),

        // public int returnNoParameters() 
        "returnNoParameters", List.of(new OrderedParameters()),

        // public void noReturnWithParameters(final @JSONRPCParameter("s") String s)
        "noReturnWithParameters", List.of(
            OrderedParameters.from(List.of(new String())),
            OrderedParameters.from(Arrays.asList((String)null))
        ),

        // public String returnWithParameters(final @JSONRPCParameter("s") String s)
        "returnWithParameters", List.of(
            OrderedParameters.from(List.of(new String())),
            OrderedParameters.from(Arrays.asList((String)null))
        ),

        // public void noReturnVariadic(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i, final @JSONRPCParameter("in") int ... in)
        "noReturnVariadic", List.of(
            OrderedParameters.from(List.of(new String(), 0)),
            OrderedParameters.from(Arrays.asList((String)null, 0)),
            OrderedParameters.from(List.of(new String(), 0, 1, 1, 2)),
            OrderedParameters.from(Arrays.asList(null, 0, new int[]{1, 1, 2})),
            OrderedParameters.from(Arrays.asList(null, 0, new int[0])),
            OrderedParameters.from(List.of(new String(), 0, new int[]{1, 1, 2})),
            OrderedParameters.from(List.of(new String(), 0, new int[0])),

            // public void noReturnVariadic(final @JSONRPCParameter("in") int ... in)
            new OrderedParameters(),
            OrderedParameters.from(List.of(0)),
            OrderedParameters.from(List.of(0, 1, 1, 2)),
            OrderedParameters.from(List.of(new int[]{0, 1, 1, 2})),
            OrderedParameters.from(List.of(new int[0]))
        ),

        // public int returnVariadic(final @JSONRPCParameter("in") int ... in)
        "returnVariadic", List.of(
            new OrderedParameters(),
            OrderedParameters.from(List.of(0)),
            OrderedParameters.from(List.of(0, 1, 1, 2))
        ),

        //public int unambiguous(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i)
        "unambiguous", List.of(
            OrderedParameters.from(List.of(new String(), 0)),
            OrderedParameters.from(Arrays.asList((String)null, 0)),

            //public void unambiguous(final @JSONRPCParameter("i") int i, final @JSONRPCParameter("s") String s)
            OrderedParameters.from(List.of(0, new String())),
            OrderedParameters.from(Arrays.asList(0, (String)null))
        ),

        //public void namedParametersAmbiguous(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("o") Object o) 
        "namedParametersAmbiguous", List.of(
            OrderedParameters.from(Arrays.asList((String)null, new Object())),
            OrderedParameters.from(List.of(new String(), new Object())),

            //public void namedParametersAmbiguous(final @JSONRPCParameter("o") Object o, final @JSONRPCParameter("s") String s) 
            OrderedParameters.from(Arrays.asList(new Object(), (String)null)),
            OrderedParameters.from(List.of(new Object(), new String()))
        )
    );

    public static final Map<String, List<NamedParameters>> NAMED_PARAMETERS_BY_METHOD_NAME = Map.of(
        // public void noReturnNoParameters()
        "noReturnNoParameters", List.of(new NamedParameters()),

        // public int returnNoParameters() 
        "returnNoParameters", List.of(new NamedParameters()),

        // public void noReturnWithParameters(final @JSONRPCParameter("s") String s)
        "noReturnWithParameters", List.of(
            NamedParameters.from(Map.of("s", new String())),
            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
            }})
        ),

        // public String returnWithParameters(final @JSONRPCParameter("s") String s)
        "returnWithParameters", List.of(
            NamedParameters.from(Map.of("s", new String())),
            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
            }})
        ),

        // public void noReturnVariadic(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i, final @JSONRPCParameter("in") int ... in)
        "noReturnVariadic", List.of(
            NamedParameters.from(Map.of("s", new String(), "i", 0)),
            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
                put("i", 0);
            }}),
            NamedParameters.from(Map.of("s", new String(), "i", 0, "in", 0)),
            NamedParameters.from(Map.of("s", new String(), "i", 0, "in", new int[]{1, 1, 2})),
            NamedParameters.from(Map.of("s", new String(), "i", 0, "in", new int[0])),

            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
                put("i", 0);
                put("in", 0);
            }}),

            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
                put("i", 0);
                put("in", new int[]{1, 1, 2});
            }}),

            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
                put("i", 0);
                put("in", new int[0]);
            }}),

            // public void noReturnVariadic(final @JSONRPCParameter("in") int ... in)
            new NamedParameters(),
            NamedParameters.from(Map.of("in", 0)),
            NamedParameters.from(Map.of("in",new int[]{0, 1, 1, 2})),
            NamedParameters.from(Map.of("in",new int[0]))
        ),

        // public int returnVariadic(final @JSONRPCParameter("in") int ... in)
        "returnVariadic", List.of(
            new NamedParameters(),
            NamedParameters.from(Map.of("in", 0)),
            NamedParameters.from(Map.of("in", new int[]{0, 1, 1, 2})),
            NamedParameters.from(Map.of("in", new int[0]))
        ),

        //public int unambiguous(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i)
        "unambiguous", List.of(
            NamedParameters.from(Map.of("s", new String(), "i", 0)),
            NamedParameters.from(new HashMap<String, Object>() {{
                put("s", (String)null);
                put("i", 0);
            }})
        )
    );

    public final static List<String> METHOD_NAMES_WITH_NO_PARAMETERS = List.of(
        "noReturnNoParameters",
        "returnNoParameters",
        "noReturnVariadic",
        "returnVariadic"
    );

    public Stream<Request> createNotificationStream() {
        return METHOD_NAMES_WITH_NO_PARAMETERS.stream().map(m -> new Request(m));
    }

    public Stream<OrderedParametersRequest> createOrderedParameterNotificationStream() {
        return ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> e.getValue().stream()
                .map(p -> new OrderedParametersRequest(e.getKey(), p))
            )
            .flatMap(i -> i);
    }

    public Stream<NamedParametersRequest> createNamedParameterNotificationStream() {
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> e.getValue().stream()
                .map(p -> new NamedParametersRequest(e.getKey(), p))
            )
            .flatMap(i -> i);
    }

    public Stream<Request> createRequestStream(final String id) {
        return METHOD_NAMES_WITH_NO_PARAMETERS.stream().map(m -> new Request(id, m));
    }

    public Stream<Request> createRequestStream(final Number id) {
        return METHOD_NAMES_WITH_NO_PARAMETERS.stream().map(m -> new Request(id, m));
    }

    public Stream<NamedParametersRequest> createNamedParameterRequestStream(final String id) {
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> e.getValue().stream()
                .map(p -> new NamedParametersRequest(id, e.getKey(), p))
            )
            .flatMap(i -> i);
    }

    public Stream<NamedParametersRequest> createNamedParameterRequestStream(final Number id) {
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> e.getValue().stream()
                .map(p -> new NamedParametersRequest(id, e.getKey(), p))
            )
            .flatMap(i -> i);
    }

    public Stream<OrderedParametersRequest> createOrderedParameterRequestStream(final String id) {
        return ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> e.getValue().stream()
                .map(p -> new OrderedParametersRequest(id, e.getKey(), p))
            )
            .flatMap(i -> i);
    }

    public Stream<OrderedParametersRequest> createOrderedParameterRequestStream(final Number id) {
        return ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> e.getValue().stream()
                .map(p -> new OrderedParametersRequest(id, e.getKey(), p))
            )
            .flatMap(i -> i);
    }
}