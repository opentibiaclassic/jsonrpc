package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.function.Executable;

import java.util.stream.Stream;
import java.util.Map;
import java.util.List;
import java.util.Arrays;

import net.opentibiaclassic.jsonrpc.Dispatcher;
import net.opentibiaclassic.jsonrpc.OrderedParameters;
import net.opentibiaclassic.jsonrpc.NamedParameters;

import static test.net.opentibiaclassic.Util.Streams.concat;

public class InvalidMethodNameProvider extends RequestBuilderArgumentsProvider {

    private static final Map<String, List<OrderedParameters>> ORDERED_PARAMETERS_BY_METHOD_NAME = Map.of(
        "foo", List.of(
            new OrderedParameters(),
            OrderedParameters.from(List.of(new String())),
            OrderedParameters.from(Arrays.asList((Object)null))
        ),

        "TestRPCServer", List.of(new OrderedParameters())
    );

    private static final Map<String, List<NamedParameters>> NAMED_PARAMETERS_BY_METHOD_NAME = Map.of(
        "foo", Arrays.asList(
            new NamedParameters(),
            NamedParameters.from(Map.of("s", new String())),
            (NamedParameters)null
        ),

        "TestRPCServer", Arrays.asList(
            new NamedParameters(),
            (NamedParameters)null
        )
    );

    private final static List<String> METHOD_NAMES_WITH_NO_PARAMETERS = List.of(
        "foo",
        "TestRPCServer"
    );

    public Stream<Executable> createOrderedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        return concat(
            ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
                .map(e -> {
                    final Stream<Executable> s =  e.getValue().stream()
                        .map(p -> () -> builder.addNotification(e.getKey(), p.toArray()));

                    return s;
                })
                .flatMap(i -> i),
            METHOD_NAMES_WITH_NO_PARAMETERS.stream()
                .map(m -> () -> builder.addNotification(m))
        );
    }

    public Stream<Executable> createNamedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> {
                final Stream<Executable> s = e.getValue().stream()
                    .map(p -> () -> builder.addNotification(e.getKey(), p));

                return s;
            })
            .flatMap(i -> i);
    }

    private Stream<Executable> createNamedParameterRequestStream(final Object id, final Dispatcher.RequestBuilder builder) {
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> {
                final Stream<Executable> s = e.getValue().stream()
                    .map(p -> (() -> {
                            if (id instanceof String) {
                                builder.addRequest((String)id, e.getKey(), p);
                            } else {
                                builder.addRequest((Number)id, e.getKey(), p);
                            }
                        })
                    );

                return s;
            })
            .flatMap(i -> i);
    }

    public Stream<Executable> createNamedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return createNamedParameterRequestStream((Object)id, builder);
    }

    public Stream<Executable> createNamedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return createNamedParameterRequestStream((Object)id, builder);
    }


    private Stream<Executable> createOrderedParameterRequestStream(final Object id, final Dispatcher.RequestBuilder builder) {
        return concat(
            ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
                .map(e -> {
                    final Stream<Executable> s =  e.getValue().stream()
                        .map(p -> (() -> {
                                if (id instanceof String) {
                                    builder.addRequest((String)id, e.getKey(), p.toArray());
                                } else {
                                    builder.addRequest((Number)id, e.getKey(), p.toArray());
                                }
                            })
                        );

                    return s;
                })
                .flatMap(i -> i),
            METHOD_NAMES_WITH_NO_PARAMETERS.stream()
                .map(m -> (() -> {
                        if (id instanceof String) {
                            builder.addRequest((String)id, m);
                        } else {
                            builder.addRequest((Number)id, m);
                        }
                    })
                )
        );
    }


    public Stream<Executable> createOrderedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return createOrderedParameterRequestStream((Object)id, builder);
    }

    public Stream<Executable> createOrderedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return createOrderedParameterRequestStream((Object)id, builder);
    }
}