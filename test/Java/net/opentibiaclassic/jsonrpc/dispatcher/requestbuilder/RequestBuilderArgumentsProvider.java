package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.api.function.Executable;

import java.util.stream.Stream;
import java.util.Arrays;
import java.util.Map;

import static test.net.opentibiaclassic.Util.Streams.concat;
import static test.Java.net.opentibiaclassic.jsonrpc.dispatcher.Const.*;

import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.TestRPCServer;

import net.opentibiaclassic.jsonrpc.Dispatcher;

public abstract class RequestBuilderArgumentsProvider implements ArgumentsProvider {

    public abstract Stream<Executable> createOrderedParameterNotificationStream(final Dispatcher.RequestBuilder builder);
    public abstract Stream<Executable> createNamedParameterNotificationStream(final Dispatcher.RequestBuilder builder);

    private Stream<Executable> createNotificationStream(final Dispatcher.RequestBuilder builder) {
        return concat(
            this.createOrderedParameterNotificationStream(builder),
            this.createNamedParameterNotificationStream(builder)
        );
    }

    public abstract Stream<Executable> createNamedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder);
    public abstract Stream<Executable> createNamedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder);
    public abstract Stream<Executable> createOrderedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder);
    public abstract Stream<Executable> createOrderedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder);

    private Stream<Executable> createRequestStream(final String[] ids, final Dispatcher.RequestBuilder builder) {
        return concat(
            Arrays.stream(ids).map(id -> createNamedParameterRequestStream(id,builder)).flatMap(i -> i),
            Arrays.stream(ids).map(id -> createOrderedParameterRequestStream(id,builder)).flatMap(i -> i)
        );
    }

    private Stream<Executable> createRequestStream(final Number[] ids, final Dispatcher.RequestBuilder builder) {
        return concat(
            Arrays.stream(ids).map(id -> createNamedParameterRequestStream(id,builder)).flatMap(i -> i),
            Arrays.stream(ids).map(id -> createOrderedParameterRequestStream(id,builder)).flatMap(i -> i)
        );
    }

    private Stream<Executable> createRequestStream(final Dispatcher.RequestBuilder builder) {
        return concat(
            createRequestStream(VALID_STRING_IDS, builder),
            createRequestStream(VALID_NUMBER_IDS, builder)
        );
    }

    private Stream<Executable> createStream() {
        final TestRPCServer server = new TestRPCServer();
        final Dispatcher.RequestBuilder builder = server.getRequestBuilder();

        return concat(
            createNotificationStream(builder),
            createRequestStream(builder)
        );
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return this.createStream().map(Arguments::of);
    }
}