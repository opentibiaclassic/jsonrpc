package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.function.Executable;

import java.util.stream.Stream;
import java.util.Map;

import net.opentibiaclassic.jsonrpc.Dispatcher;

public class AmbiguousMethodExceptionProvider extends RequestBuilderArgumentsProvider {

    private static final Map DEFAULT_MAP = Map.of(
        "s", new String(),
        "o", new Object()
    );
    private static final Object[] DEFAULT_PARAMETERS = new Object[] { 
        new String(),
        null 
    };

    public Stream<Executable> createOrderedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        return Stream.of(
            () -> builder.addNotification("ambiguous", DEFAULT_PARAMETERS)
        );
    }

    public Stream<Executable> createNamedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        return Stream.of(
            () -> builder.addNotification("namedParametersAmbiguous", DEFAULT_MAP)
        );
    }

    public Stream<Executable> createNamedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return Stream.of(
            () -> builder.addRequest(id, "namedParametersAmbiguous", DEFAULT_MAP)
        );
    }

    public Stream<Executable> createNamedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return Stream.of(
            () -> builder.addRequest(id, "namedParametersAmbiguous", DEFAULT_MAP)
        );
    }

    public Stream<Executable> createOrderedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return Stream.of(
            () -> builder.addRequest(id, "ambiguous", DEFAULT_PARAMETERS)
        );
    }

    public Stream<Executable> createOrderedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return Stream.of(
            () -> builder.addRequest(id, "ambiguous", DEFAULT_PARAMETERS)
        );
    }
}