package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;
import java.util.Arrays;
import java.util.Map;

import static test.net.opentibiaclassic.Util.Streams.concat;
import static test.Java.net.opentibiaclassic.jsonrpc.dispatcher.Const.*;

import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.TestRPCServer;

import net.opentibiaclassic.jsonrpc.Dispatcher;
import net.opentibiaclassic.jsonrpc.Request;
import net.opentibiaclassic.jsonrpc.NamedParametersRequest;
import net.opentibiaclassic.jsonrpc.OrderedParametersRequest;

public abstract class RequestArgumentsProvider implements ArgumentsProvider {

    public abstract Stream<Request> createNotificationStream();
    public abstract Stream<OrderedParametersRequest> createOrderedParameterNotificationStream();
    public abstract Stream<NamedParametersRequest> createNamedParameterNotificationStream();

    public abstract Stream<Request> createRequestStream(final String id);
    public abstract Stream<Request> createRequestStream(final Number id);
    public abstract Stream<NamedParametersRequest> createNamedParameterRequestStream(final String id);
    public abstract Stream<NamedParametersRequest> createNamedParameterRequestStream(final Number id);
    public abstract Stream<OrderedParametersRequest> createOrderedParameterRequestStream(final String id);
    public abstract Stream<OrderedParametersRequest> createOrderedParameterRequestStream(final Number id);


    private Stream<Request> createRequestStream(final String[] ids) {
        return concat(
            Arrays.stream(ids).map(id -> createRequestStream(id)).flatMap(i -> i),
            Arrays.stream(ids).map(id -> createNamedParameterRequestStream(id)).flatMap(i -> i),
            Arrays.stream(ids).map(id -> createOrderedParameterRequestStream(id)).flatMap(i -> i)
        );
    }

    private Stream<Request> createRequestStream(final Number[] ids) {
        return concat(
            Arrays.stream(ids).map(id -> createRequestStream(id)).flatMap(i -> i),
            Arrays.stream(ids).map(id -> createNamedParameterRequestStream(id)).flatMap(i -> i),
            Arrays.stream(ids).map(id -> createOrderedParameterRequestStream(id)).flatMap(i -> i)
        );
    }

    private Stream<Request> createStream() {
        return concat(
            createNotificationStream(),
            createOrderedParameterNotificationStream(),
            createNamedParameterNotificationStream(),

            createRequestStream(VALID_STRING_IDS),
            createRequestStream(VALID_NUMBER_IDS)
        );
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return this.createStream().map(Arguments::of);
    }
}