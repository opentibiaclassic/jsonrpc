package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.function.Executable;

import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Map;
import java.util.List;
import java.util.Objects;
import java.util.Collection;

import net.opentibiaclassic.jsonrpc.Dispatcher;
import net.opentibiaclassic.jsonrpc.OrderedParameters;
import net.opentibiaclassic.jsonrpc.NamedParameters;

import static test.net.opentibiaclassic.Util.not;
import static test.net.opentibiaclassic.Util.Streams.concat;
import static test.net.opentibiaclassic.Util.Streams.append;
import static test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.ValidRequestProvider.ORDERED_PARAMETERS_BY_METHOD_NAME;
import static test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.ValidRequestProvider.NAMED_PARAMETERS_BY_METHOD_NAME;

public class MissingParametersProvider extends RequestBuilderArgumentsProvider {

    private static boolean isVariadic(final String methodName) {
        return methodName.toLowerCase().contains("variadic");
    }

    private static boolean isParametersRequired(final List l) {
        return not(l.stream().anyMatch(x -> {
            if (x instanceof OrderedParameters) {
                return 0 == ((OrderedParameters)x).size();
            }

            return 0 == ((NamedParameters)x).size();
        }));
    }

    // just tries removing 1 parameter
    private static List<OrderedParameters> mapOrderedParametersToMissingParameters(final List<OrderedParameters> validParameters) {
        return validParameters.stream()
            .map(orderedParameters -> {
                final Stream<OrderedParameters> missing = orderedParameters.stream()
                    .map(parameter -> {
                        final OrderedParameters copy = OrderedParameters.from(orderedParameters);
                        copy.remove(parameter);

                        return copy;
                    });

                return missing;
            })
            .flatMap(i -> i)
            .collect(Collectors.toList());
    }

    private static List<NamedParameters> mapNamedParametersToMissingParameters(final List<NamedParameters> validParameters) {
        return validParameters.stream()
            .map(namedParameters -> {
                final Stream<NamedParameters> missing = namedParameters.keySet().stream()
                    .map(parameter -> {
                        final NamedParameters copy = NamedParameters.from(namedParameters);
                        copy.remove(parameter);

                        return copy;
                    });

                return missing;
            })
            .flatMap(i -> i)
            .collect(Collectors.toList());
    }

    public Stream<Executable> createOrderedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        // TODO test the variadic method noReturnVariadic
        return ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .filter(e -> isParametersRequired(e.getValue()))
            .filter(e -> not(isVariadic(e.getKey())))
            .map(e -> {
                final String methodName = e.getKey();
                final List<OrderedParameters> incompleteParameters = mapOrderedParametersToMissingParameters(e.getValue());

                final Stream<Executable> s = incompleteParameters.stream()
                    .map(p -> () -> builder.addNotification(methodName, p.toArray()));

                return append(s, () -> builder.addNotification(methodName));
            })
            .flatMap(i -> i);
    }

    public Stream<Executable> createNamedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        // TODO test the variadic method noReturnVariadic
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .filter(e -> isParametersRequired(e.getValue()))
            .filter(e -> not(isVariadic(e.getKey())))
            .map(e -> {
                final String methodName = e.getKey();
                final List<NamedParameters> incompleteParameters = mapNamedParametersToMissingParameters(e.getValue());

                final Stream<Executable> s = incompleteParameters.stream()
                    .map(p -> () -> builder.addNotification(methodName, p));

                return s;
            })
            .flatMap(i -> i);
    }

    private Stream<Executable> createNamedParameterRequestStream(final Object id, final Dispatcher.RequestBuilder builder) {
        return NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .filter(e -> isParametersRequired(e.getValue()))
            .filter(e -> not(isVariadic(e.getKey())))
            .map(e -> {
                final String methodName = e.getKey();
                final List<NamedParameters> incompleteParameters = mapNamedParametersToMissingParameters(e.getValue());

                final Stream<Executable> s;
                if (id instanceof String) {
                    s = incompleteParameters.stream()
                        .map(p -> () -> builder.addRequest((String)id, methodName, p));
                } else {
                    s = incompleteParameters.stream()
                        .map(p -> () -> builder.addRequest((Number)id, methodName, p));
                }

                return s;
            })
            .flatMap(i -> i);
    }

    public Stream<Executable> createNamedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return createNamedParameterRequestStream((Object)id, builder);
    }

    public Stream<Executable> createNamedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return createNamedParameterRequestStream((Object)id, builder);
    }

    private Stream<Executable> createOrderedParameterRequestStream(final Object id, final Dispatcher.RequestBuilder builder) {
        return ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .filter(e -> isParametersRequired(e.getValue()))
            .filter(e -> not(isVariadic(e.getKey())))
            .map(e -> {
                final String methodName = e.getKey();
                final List<OrderedParameters> incompleteParameters = mapOrderedParametersToMissingParameters(e.getValue());

                if (id instanceof String) {
                    final Stream<Executable> s = incompleteParameters.stream()
                        .map(p -> () -> builder.addRequest((String)id, methodName, p.toArray()));

                    return append(s, () -> builder.addRequest((String)id, methodName));
                }

                final Stream<Executable> s = incompleteParameters.stream()
                    .map(p -> () -> builder.addRequest((Number)id, methodName, p.toArray()));

                return append(s, () -> builder.addRequest((Number)id, methodName));

            })
            .flatMap(i -> i);
    }

    public Stream<Executable> createOrderedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return createOrderedParameterRequestStream((Object)id, builder);
    }

    public Stream<Executable> createOrderedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return createOrderedParameterRequestStream((Object)id, builder);
    }
}