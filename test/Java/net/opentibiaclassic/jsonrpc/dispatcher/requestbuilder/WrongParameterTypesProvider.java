package test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder;

import org.junit.jupiter.api.function.Executable;

import java.util.stream.Stream;
import java.util.Collections;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;

import static test.net.opentibiaclassic.Util.makeList;

import net.opentibiaclassic.jsonrpc.Dispatcher;

public class WrongParameterTypesProvider extends RequestBuilderArgumentsProvider {

    private static final Map<String, List<Map>> INVALID_NAMED_PARAMETERS_BY_METHOD_NAME = Map.of(
        "noReturnVariadic", List.of(
            Map.of("s", new String(), "i", 0, "in", new Double[]{0.0,1.0,1.0,2.0}),
            Map.of("s", new String(), "i", "not-an-int", "in", new int[]{0,1,1,2}),
            Map.of("s", new Object(), "i", 0, "in", new int[]{0,1,1,2}),
            Map.of("s", new String(), "i", "not-an-int"),
            Map.of("s", new Object(), "i", 0),
            Map.of("in", new String[0])
        ),
        "noReturnWithParameters", List.of(
            Map.of("s", new Object())
        ),
        "returnWithParameters", List.of(
            Map.of("s", new Object())
        ),
        "returnVariadic", List.of(
            Map.of("in", new String[0])
        ),
        "unambiguous", List.of(
            Map.of("s", new Object(), "o", new Object()),
            Map.of("s", new Object(), "i", 0),
            Map.of("s", new String(), "i", 0.0)
        ),
        "namedParametersAmbiguous", List.of(
            Map.of("s", new Object(), "o", new Object())
        )
    );

    private static final Map<String, List<Object[]>> INVALID_ORDERED_PARAMETERS_BY_METHOD_NAME = Map.of(
        "ambiguous", makeList(new Object[] { new Object(), new Object()}),
        "noReturnWithParameters", makeList(new Object[0]),
        "returnWithParameters", makeList(new Object[0]),
        "noReturnVariadic", makeList(new Object[] { new String() }),
        "unambiguous", makeList(
            new Object[] { new String(), new String() },
            new Object[] { 0, 0 }
        ),
        "namedParametersAmbiguous", makeList(new Object[] { new Object(), new Object() })
    );

    private static <T> Stream<Executable> createNotificationStream(final Dispatcher.RequestBuilder builder, final Map.Entry<String, List<T>> entry) {
        final String method = entry.getKey();
        final Stream<Executable> s =  entry.getValue().stream()
            .map(p -> {
                if (p instanceof Map) {
                    return (() -> builder.addNotification(method, (Map)p));
                }

                return (() -> builder.addNotification(method, (Object[])p));
            });

        return s;
    }

    public Stream<Executable> createOrderedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        return INVALID_ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> createNotificationStream(builder, e)).flatMap(i -> i);
    }

    public Stream<Executable> createNamedParameterNotificationStream(final Dispatcher.RequestBuilder builder) {
        return INVALID_NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> createNotificationStream(builder, e)).flatMap(i -> i);
    }

    private static <T> Stream<Executable> createNamedParameterRequestStream(final Dispatcher.RequestBuilder builder, final Object id, final Map.Entry<String, List<T>> entry) {
        final String method = entry.getKey();
        final Stream<Executable> s =  entry.getValue().stream()
            .map(p -> {
                if (p instanceof Map) {
                    if (id instanceof String) {
                        return (() -> builder.addRequest((String)id, method, (Map)p));
                    }
                    
                    return (() -> builder.addRequest((Number)id, method, (Map)p));
                }

                if (id instanceof String) {
                    return (() -> builder.addRequest((String)id, method, (Object[])p));
                }
                    
                return (() -> builder.addRequest((Number)id, method, (Object[])p));
            });

        return s;
    }

    public Stream<Executable> createNamedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return INVALID_NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> createNamedParameterRequestStream(builder, id, e)).flatMap(i -> i);
    }

    public Stream<Executable> createNamedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return INVALID_NAMED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> createNamedParameterRequestStream(builder, id, e)).flatMap(i -> i);
    }

    public Stream<Executable> createOrderedParameterRequestStream(final String id, final Dispatcher.RequestBuilder builder) {
        return INVALID_ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> createNamedParameterRequestStream(builder, id, e)).flatMap(i -> i);
    }

    public Stream<Executable> createOrderedParameterRequestStream(final Number id, final Dispatcher.RequestBuilder builder) {
        return INVALID_ORDERED_PARAMETERS_BY_METHOD_NAME.entrySet().stream()
            .map(e -> createNamedParameterRequestStream(builder, id, e)).flatMap(i -> i);
    }
}