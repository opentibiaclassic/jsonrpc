package test.Java.net.opentibiaclassic.jsonrpc.dispatcher;

import java.util.Map;
import java.util.List;
import java.util.Arrays;

import net.opentibiaclassic.jsonrpc.Dispatcher;
import net.opentibiaclassic.jsonrpc.JSONRPCParameter;

    public class TestRPCServer extends Dispatcher {
        public final Object returnedObject;
        public final String returnedString;
        public final Number returnedNumber;
        public final Map returnedMap;
        public final TestBean returnedBean;
        public final List returnedList;

        public TestRPCServer() {
            super();
            this.returnedObject = null;
            this.returnedString = null;
            this.returnedNumber = null;
            this.returnedMap = null;
            this.returnedBean = null;
            this.returnedList = null;
        }

        public TestRPCServer(final Object o, final String s, final Number n, final Map m, final TestBean b, final List l) {
            this.returnedObject = o;
            this.returnedString = s;
            this.returnedNumber = n;
            this.returnedMap = m;
            this.returnedBean = b;
            this.returnedList = l;
        }

        public void ambiguous(final String s, final Object o) {

        }

        public void ambiguous(final Object o, final String s) {
            
        }

        public void noReturnNoParameters() {

        }

        public int returnNoParameters() {
            return 0;
        }

        public void noReturnWithParameters(final @JSONRPCParameter("s") String s) {

        }

        public String returnWithParameters(final @JSONRPCParameter("s") String s) {
            return s;
        }

        public void noReturnVariadic(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i, final @JSONRPCParameter("in") int ... in) {

        }

        public void noReturnVariadic(final @JSONRPCParameter("in") int ... in) {

        }

        public int returnVariadic(final @JSONRPCParameter("in") int ... in) {
            return Arrays.stream(in).reduce(0, Integer::sum);
        }

        public int unambiguous(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("i") int i) {
            return 0;
        }

        public void unambiguous(final @JSONRPCParameter("i") int i, final @JSONRPCParameter("s") String s) {

        }

        public void namedParametersAmbiguous(final @JSONRPCParameter("s") String s, final @JSONRPCParameter("o") Object o) {

        }

        public void namedParametersAmbiguous(final @JSONRPCParameter("o") Object o, final @JSONRPCParameter("s") String s) {
            
        }

        public Object returnNull() {
            return null;
        }

        public String returnString() {
            return returnedString;
        }

        public boolean returnFalse() {
            return false;
        }

        public boolean returnTrue() {
            return true;
        }

        public Number returnNumber() {
            return returnedNumber;
        }

        public Map returnMap() {
            return returnedMap;
        }

        public Object returnObject() {
            return returnedObject;
        }

        public Object returnBean() {
            return returnedBean;
        }

        public List returnList() {
            return returnedList;
        }
    }