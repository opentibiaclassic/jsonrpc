package test.net.opentibiaclassic;

import java.util.stream.Stream;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;

public class Util {
    public static class Streams {
        public static <T> Stream<T> concat(Stream<? extends T> ... streams) {
            return Stream.of(streams).flatMap(i->i);
        }

        public static <T> Stream<T> append(Stream<? extends T> stream, T ... e) {
            return concat(stream, Stream.of(e));
        }
    }

    public static class Math {

        public static class InclusiveRange<T extends Comparable> {
            private final T minima, maxima;

            public InclusiveRange(final T a, final T b) {
                if (a.compareTo(b) <= 0) {
                    minima = a;
                    maxima = b;
                } else {
                    minima = b;
                    maxima = a;
                }
            }

            public boolean contains(final T v) {
                return minima.compareTo(v) <= 0
                    && maxima.compareTo(v) >= 0;
            }
        }
    }

    public static boolean not(final boolean a) {
        return !a;
    }

    // needed because List.of(Object[]) spreads the argument
    public static List<Object[]> makeList(Object[] ... args) {
        List<Object[]> l = new LinkedList<Object[]>();

        for (Object[] item : args) {
            l.add(item);
        }

        return Collections.unmodifiableList(l);
    }
}