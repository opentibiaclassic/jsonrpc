package test.Java.net.opentibiaclassic.jsonrpc;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;

import java.util.stream.Stream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import net.opentibiaclassic.jsonrpc.Response;
import net.opentibiaclassic.jsonrpc.Success;
import net.opentibiaclassic.jsonrpc.Result;
import net.opentibiaclassic.jsonrpc.Error;
import net.opentibiaclassic.jsonrpc.errors.*;

class ResponseTests {

    @BeforeAll
    static void initAll() {

    }

    @BeforeEach
    void init() {

    }

    /*
        Response object

        When a rpc call is made, the Server MUST reply with a Response, except for in the case of Notifications. The Response is expressed as a single JSON Object, with the following members:

        jsonrpc
            A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".

        result
            This member is REQUIRED on success.
            This member MUST NOT exist if there was an error invoking the method.
            The value of this member is determined by the method invoked on the Server.

        error
            This member is REQUIRED on error.
            This member MUST NOT exist if there was no error triggered during invocation.
            The value for this member MUST be an Object as defined in section 5.1.

        id
            This member is REQUIRED.
            It MUST be the same as the value of the id member in the Request Object.
            If there was an error in detecting the id in the Request object (e.g. Parse error/Invalid Request), it MUST be Null.

        Either the result member or error member MUST be included, but both members MUST NOT be included.
        5.1 Error object

        When a rpc call encounters an error, the Response Object MUST contain the error member with a value that is a Object with the following members:

        code
            A Number that indicates the error type that occurred.
            This MUST be an integer.
        message
            A String providing a short description of the error.
            The message SHOULD be limited to a concise single sentence.
        data
            A Primitive or Structured value that contains additional information about the error.
            This may be omitted.
            The value of this member is defined by the Server (e.g. detailed error information, nested errors etc.).

        The error codes from and including -32768 to -32000 are reserved for pre-defined errors. Any code within this range, but not defined explicitly below is reserved for future use. The error codes are nearly the same as those suggested for XML-RPC at the following url: http://xmlrpc-epi.sourceforge.net/specs/rfc.fault_codes.php
        code    message     meaning
        -32700  Parse error     Invalid JSON was received by the server.
        An error occurred on the server while parsing the JSON text.
        -32600  Invalid Request     The JSON sent is not a valid Request object.
        -32601  Method not found    The method does not exist / is not available.
        -32602  Invalid params  Invalid method parameter(s).
        -32603  Internal error  Internal JSON-RPC error.
        -32000 to -32099    Server error    Reserved for implementation-defined server-errors.
    */
    /*
        Success Tests
    */

    static Stream<Object> createAnyValidJSONValueStream() {
        // null, false, true, number, string, object, array, map, list
        return Stream.of(
            null,
            new Object(),
            JSONObject.NULL,
            new JSONObject(),
            new JSONArray(),
            "",
            new String(),
            (Integer)0,
            Integer.MAX_VALUE,
            Integer.MIN_VALUE,
            0,
            1,
            0.0,
            1.5,
            false,
            true,
            Map.of(),
            Map.of("abc", 123),
            List.of(),
            List.of("a","b","c", 1, 2, 3)
        );
    }

    @DisplayName("Success(final Object id) constructor")
    @ParameterizedTest
    @MethodSource("createAnyValidJSONValueStream")
    void successTest(final Object id) {
        // public Success(final Object id)
        final Success response = new Success(id);

        assertEquals(id, response.getId());

        final JSONObject obj = new JSONObject(response.toJSON());
        
        assertTrue(obj.has("id"));
        assertFalse(obj.has("result"));
        assertFalse(obj.has("error"));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    static Stream<Result> createResultStream() {
        return createAnyValidJSONValueStream()
            .map(id -> createAnyValidJSONValueStream()
                .map(v -> new Result(id, v))
            ).flatMap(i -> i);
    }


    /*
        Result Tests
    */
    @DisplayName("Result(final Object id, final Object result) constructor")
    @ParameterizedTest
    @MethodSource("createResultStream")
    void resultTest(final Result response) {
        // public Result(final Object id, final Object result)
        final JSONObject obj = new JSONObject(response.toJSON());
        
        assertTrue(obj.has("id"));
        assertTrue(obj.has("result"));
        assertFalse(obj.has("error"));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    /*
        Error Tests
    */

    static Stream<JSONRPCException> createJSONRPCExceptionStream() {
        return Stream.of(
            new InvalidRequestError(null),
            new MethodNotFoundError((Object)null),
            new ParseError(new JSONException("parse error"), null),
            new ServerError(new Exception())
        );
    }

    static Stream<Error> createErrorStream() {
        return createJSONRPCExceptionStream()
            .map(ex -> new Error(ex));
    }

    @DisplayName("Error(final JSONRPCException exception) constructor")
    @ParameterizedTest
    @MethodSource("createErrorStream")
    void errorTest(final Error response) {
        // public Error(final JSONRPCException exception)
        final JSONObject obj = new JSONObject(response.toJSON());
        
        assertTrue(obj.has("id"));
        assertFalse(obj.has("result"));
        assertTrue(obj.has("error"));
        assertEquals("2.0", obj.getString("jsonrpc"));

        final JSONObject error = obj.getJSONObject("error");
        assertTrue(error.has("code"));
        final Number code = error.getNumber("code");
        assertTrue(code instanceof Integer || code instanceof BigInteger);

        if (error.has("message")) {
            error.getString("message");
        }
    }

    static Stream<Error> createErrorWithIdStream() {
        return createAnyValidJSONValueStream()
            .map(id -> createJSONRPCExceptionStream()
                .map(ex -> new Error(ex, id))
            ).flatMap(i -> i);
    }

    @DisplayName("Error(final JSONRPCException exception, final Object id) constructor")
    @ParameterizedTest
    @MethodSource("createErrorWithIdStream")
    void errorWithIdTest(final Error response) {
        // public Error(final JSONRPCException exception, final Object id)
        errorTest(response);
    }
}