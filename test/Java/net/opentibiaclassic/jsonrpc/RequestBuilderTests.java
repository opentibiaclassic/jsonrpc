package test.Java.net.opentibiaclassic.jsonrpc;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.function.Executable;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.math.BigInteger;

import java.util.stream.Stream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.concurrent.Callable;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import net.opentibiaclassic.jsonrpc.AmbiguousMethodException;
import net.opentibiaclassic.jsonrpc.Dispatcher;
import net.opentibiaclassic.jsonrpc.Request;
import net.opentibiaclassic.jsonrpc.NamedParameters;
import net.opentibiaclassic.jsonrpc.NamedParametersRequest;
import net.opentibiaclassic.jsonrpc.OrderedParametersRequest;

import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.ValidRequestProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.AmbiguousMethodExceptionProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.MissingParametersProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.WrongParameterTypesProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.InvalidMethodNameProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.TestRPCServer;

class RequestBuilderTests {

    @BeforeAll
    static void initAll() {

    }

    @BeforeEach
    void init() {

    }

    /*
        RequestBuilder Tests
    */
    @ParameterizedTest
    @ArgumentsSource(AmbiguousMethodExceptionProvider.class)
    void ambiguousRequestBuilderTest(final Executable e) {
        assertThrows(AmbiguousMethodException.class, e);
    }

    @ParameterizedTest
    @ArgumentsSource(MissingParametersProvider.class)
    @ArgumentsSource(WrongParameterTypesProvider.class)
    @ArgumentsSource(InvalidMethodNameProvider.class)
    void methodNotFoundRequestBuilderTest(final Executable e) {
        assertThrows(NoSuchMethodException.class, e);
    }

    @Test
    void emptyRequestBuilderTest() {
        final TestRPCServer server = new TestRPCServer();
        final Dispatcher.RequestBuilder builder = server.getRequestBuilder();

        assertNull(builder.toJSON());
    }


    private static void addNotice(final Dispatcher.RequestBuilder builder, final OrderedParametersRequest request) throws AmbiguousMethodException, NoSuchMethodException {
        builder.addNotification(request.getMethod(), request.getParameters().toArray());
    }

    private static void addNotice(final Dispatcher.RequestBuilder builder, final NamedParametersRequest request) throws AmbiguousMethodException, NoSuchMethodException {
        builder.addNotification(request.getMethod(), request.getParameters());
    }

    private static void addNotice(final Dispatcher.RequestBuilder builder, final Request request) throws AmbiguousMethodException, NoSuchMethodException {
        if (request instanceof NamedParametersRequest) {
            addNotice(builder, (NamedParametersRequest)request);
        } else if (request instanceof OrderedParametersRequest) {
            addNotice(builder, (OrderedParametersRequest)request);
        } else {
            builder.addNotification(request.getMethod());
        }
    }

    private static void addRequest(final Dispatcher.RequestBuilder builder, final OrderedParametersRequest request) throws AmbiguousMethodException, NoSuchMethodException {
        final Object id = request.getId();
        final String method = request.getMethod();
        final Object[] parameters = request.getParameters().toArray();

        if (id instanceof String) {
            builder.addRequest((String)id, method, parameters);
        } else {
            builder.addRequest((Number)id, method, parameters);
        }
    }

    private static void addRequest(final Dispatcher.RequestBuilder builder, final NamedParametersRequest request) throws AmbiguousMethodException, NoSuchMethodException {
        final Object id = request.getId();
        final String method = request.getMethod();
        final Map parameters = request.getParameters();

        if (id instanceof String) {
            builder.addRequest((String)id, method, parameters);
        } else {
            builder.addRequest((Number)id, method, parameters);
        }
    }

    private static void addRequest(final Dispatcher.RequestBuilder builder, final Request request) throws AmbiguousMethodException, NoSuchMethodException {
        if (request instanceof NamedParametersRequest) {
            addRequest(builder, (NamedParametersRequest)request);
        } else if (request instanceof OrderedParametersRequest) {
            addRequest(builder, (OrderedParametersRequest)request);
        } else if (request.getId() instanceof String) {
            builder.addRequest((String)request.getId(), request.getMethod());
        } else {
            builder.addRequest((Number)request.getId(), request.getMethod());
        }
    }

    @ParameterizedTest
    @ArgumentsSource(ValidRequestProvider.class)
    void requestBuilderTest(final Request request) throws AmbiguousMethodException, NoSuchMethodException {
        final TestRPCServer server = new TestRPCServer();
        final Dispatcher.RequestBuilder builder = server.getRequestBuilder();

        if (request.isNotification()) {
            addNotice(builder, request);
        } else {
            addRequest(builder, request);
        }

        assertTrue(request.toJSONObject().similar(new JSONObject(builder.toJSON())));
    }

    // Note: No testing for building batch requests
}