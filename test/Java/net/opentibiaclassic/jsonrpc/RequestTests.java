package test.Java.net.opentibiaclassic.jsonrpc;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.json.JSONObject;
import org.json.JSONArray;

import net.opentibiaclassic.jsonrpc.errors.InvalidRequestError;
import net.opentibiaclassic.jsonrpc.Request;
import net.opentibiaclassic.jsonrpc.NamedParametersRequest;
import net.opentibiaclassic.jsonrpc.NamedParameters;
import net.opentibiaclassic.jsonrpc.OrderedParametersRequest;
import net.opentibiaclassic.jsonrpc.OrderedParameters;

class RequestTests {

    @BeforeAll
    static void initAll() {

    }

    @BeforeEach
    void init() {

    }

    /*

        4 Request object

        A rpc call is represented by sending a Request object to a Server. The Request object has the following members:

        jsonrpc
            A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".

        method
            A String containing the name of the method to be invoked. Method names that begin with the word rpc followed by a period character (U+002E or ASCII 46) are reserved for rpc-internal methods and extensions and MUST NOT be used for anything else.

        params
            A Structured value that holds the parameter values to be used during the invocation of the method. This member MAY be omitted.

        id
            An identifier established by the Client that MUST contain a String, Number, or NULL value if included. If it is not included it is assumed to be a notification. The value SHOULD normally not be Null [1] and Numbers SHOULD NOT contain fractional parts [2]

        The Server MUST reply with the same value in the Response object if included. This member is used to correlate the context between the two objects.

        [1] The use of Null as a value for the id member in a Request object is discouraged, because this specification uses a value of Null for Responses with an unknown id. Also, because JSON-RPC 1.0 uses an id value of Null for Notifications this could cause confusion in handling.

        [2] Fractional parts may be problematic, since many decimal fractions cannot be represented exactly as binary fractions.
        
        4.1 Notification

        A Notification is a Request object without an "id" member. A Request object that is a Notification signifies the Client's lack of interest in the corresponding Response object, and as such no Response object needs to be returned to the client. The Server MUST NOT reply to a Notification, including those that are within a batch request.

        Notifications are not confirmable by definition, since they do not have a Response object to be returned. As such, the Client would not be aware of any errors (like e.g. "Invalid params","Internal error").
        
        4.2 Parameter Structures

        If present, parameters for the rpc call MUST be provided as a Structured value. Either by-position through an Array or by-name through an Object.

            by-position: params MUST be an Array, containing the values in the Server expected order.
            by-name: params MUST be an Object, with member names that match the Server expected parameter names. The absence of expected names MAY result in an error being generated. The names MUST match exactly, including case, to the method's expected parameters.


    */
    /*
        Request Tests
    */

    @Test
    @DisplayName("Request(final String methodName) constructor")
    void constructor1Test() {
        // public Request(final String methodName)
        final String methodName = "foo";
        final String signature = String.format("%s()", methodName);

        final Request request = new Request(methodName);

        assertEquals(methodName, request.getMethod());
        assertEquals(null, request.getId());
        assertEquals(signature, request.getSignature());
        assertTrue(request.isNotification());

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    @Test
    @DisplayName("Request(final Object id, final String methodName) constructor")
    void constructor2Test() {
        // public Request(final Object id, final String methodName)
        final String methodName = "foo";
        final String id = "bar";
        final String signature = String.format("%s()", methodName);

        final Request request = new Request(id, methodName);

        assertEquals(methodName, request.getMethod());
        assertEquals(id, request.getId());
        assertEquals(signature, request.getSignature());
        assertTrue(!request.isNotification());

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    private static JSONObject createValidCall() {
        return new JSONObject()
            .put("method", "foo")
            .put("jsonrpc", "2.0");
    }

    private static final Object[] INVALID_METHOD = new Object[] {
        // method must be non-empty string
        JSONObject.NULL,
        1,
        1.5,
        0,
        0.0,
        new JSONObject(),
        new JSONArray(),
        true,
        false,
        ""
    };

    private static final Object[] INVALID_ID = new Object[] {
        // id must be NULL, String, Number
        new JSONObject(),
        new JSONArray(),
        true,
        false
    };

    private static final Object[] INVALID_JSONRPC = new Object[] {
        // jsonrpc must be exactly 2.0
        JSONObject.NULL,
        1,
        1.5,
        0,
        0.0,
        new JSONObject(),
        new JSONArray(),
        true,
        false,
        "",
        "abc"
    };

    private static final JSONObject[] INVALID_CALLS = new JSONObject[] {
        // missing 'jsonrpc'
        new JSONObject().put("method", "foo"),
        // missing 'method'
        new JSONObject().put("jsonrpc", "2.0")
    };

    private static Stream<JSONObject> createInvalidCallsFromId(Object ... values) {
        return Arrays.stream(values).map(v -> createValidCall().put("id", v));
    }

    private static Stream<JSONObject> createInvalidCallsFromMethod(Object ... values) {
        return Arrays.stream(values).map(v -> createValidCall().put("method", v));
    }

    private static Stream<JSONObject> createInvalidCallsFromJSONRPC(Object ... values) {
        return Arrays.stream(values).map(v -> createValidCall().put("jsonrpc", v));
    }

    static Stream<JSONObject> createInvalidCallStream() {
        return Stream.of(
            createInvalidCallsFromId(INVALID_ID),
            createInvalidCallsFromMethod(INVALID_METHOD),
            createInvalidCallsFromJSONRPC(INVALID_JSONRPC),
            Arrays.stream(INVALID_CALLS)
        ).flatMap(i -> i);
    }

    @DisplayName("Request(final JSONObject remoteCall) constructor throws InvalidRequestError")
    @ParameterizedTest
    @MethodSource("createInvalidCallStream")
    void constructor3Test(final JSONObject invalidCall) {
        // Request(final JSONObject remoteCall) throws InvalidRequestError
        assertThrows(InvalidRequestError.class, () -> {
            new Request(invalidCall);
        });
    }

    private static Stream<JSONObject> createValidCallsFromId(Object ... values) {
        return Arrays.stream(values).map(v -> createValidCall().put("id", v));
    }

    private static Stream<JSONObject> createValidCallsFromMethod(Object ... values) {
        return Arrays.stream(values).map(v -> createValidCall().put("method", v));
    }

    static Stream<JSONObject> createValidCallStream() {
        return Stream.of(
            createValidCallsFromId(JSONObject.NULL, "", "abc", 1, new String()),
            createValidCallsFromMethod("foo", "\nfoo", "foo\n", "\n", "rpc.extension"),
            Stream.of(createValidCall())
        ).flatMap(i -> i);
    }

    @DisplayName("Request(final JSONObject remoteCall) constructor")
    @ParameterizedTest
    @MethodSource("createValidCallStream")
    void constructor4Test(final JSONObject validCall) throws InvalidRequestError {
        // Request(final JSONObject remoteCall) throws InvalidRequestError
        final Request request = new Request(validCall);

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertTrue(obj.similar(validCall));
    }

    /*
        OrderedParametersRequest Tests
    */
    static Stream<Entry<String, OrderedParameters>> createValidOrderedParametersStream() {
        return Stream.of(
                new SimpleEntry<String, OrderedParameters>("(string)", OrderedParameters.from(List.of("bar"))),
                new SimpleEntry<String, OrderedParameters>("()", new OrderedParameters()),
                new SimpleEntry<String, OrderedParameters>("()", null)
            );
    }

    @DisplayName("OrderedParametersRequest(final String methodName, final OrderedParameters parameters) constructor")
    @ParameterizedTest
    @MethodSource("createValidOrderedParametersStream")
    void constructor5Test(final Entry<String, OrderedParameters> pair) {
        // public OrderedParametersRequest(final String methodName, final OrderedParameters parameters)
        final String methodName = "foo";
        final String signature = String.format("%s%s", methodName, pair.getKey());
        final OrderedParameters parameters = pair.getValue();
        final OrderedParametersRequest request = new OrderedParametersRequest(methodName, parameters);

        assertEquals(methodName, request.getMethod());
        assertEquals(null, request.getId());
        assertEquals(signature, request.getSignature());
        assertTrue(request.isNotification());

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    @DisplayName("OrderedParametersRequest(final Object id, final String methodName, final OrderedParameters parameters) constructor")
    @ParameterizedTest
    @MethodSource("createValidOrderedParametersStream")
    void constructor6Test(final Entry<String, OrderedParameters> pair) {
        // public OrderedParametersRequest(final Object id, final String methodName, final OrderedParameters parameters)
        final String methodName = "foo";
        final int id = 1;
        final String signature = String.format("%s%s", methodName, pair.getKey());
        final OrderedParameters parameters = pair.getValue();
        final OrderedParametersRequest request = new OrderedParametersRequest(id, methodName, parameters);

        assertEquals(methodName, request.getMethod());
        assertEquals(id, request.getId());
        assertEquals(signature, request.getSignature());
        assertFalse(request.isNotification());

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    static Stream<JSONObject> createInvalidOrderedParametersCallStream() {
        return Stream.of(
            createInvalidCallStream().map(x -> x.put("params", new OrderedParameters())),
            createInvalidCallStream().map(x -> x.put("params", OrderedParameters.from(List.of("bar")))),
            createValidCallStream().map(x -> x.put("params", "params")),
            createValidCallStream().map(x -> x.put("params", 1)),
            createValidCallStream().map(x -> x.put("params", true)),
            createValidCallStream().map(x -> x.put("params", false)),
            createValidCallStream().map(x -> x.put("params", JSONObject.NULL)),
            createValidCallStream().map(x -> x.put("params", new JSONObject()))
            ).flatMap(i -> i);
    }

    @DisplayName("OrderedParametersRequest(final JSONObject remoteCall) constructor throws InvalidRequestError")
    @ParameterizedTest
    @MethodSource("createInvalidOrderedParametersCallStream")
    void constructor7Test(final JSONObject invalidCall) {
        // OrderedParametersRequest(final JSONObject remoteCall) throws InvalidRequestError
        assertThrows(InvalidRequestError.class, () -> {
            new OrderedParametersRequest(invalidCall);
        });
    }

    static Stream<JSONObject> createValidOrderedParametersCallStream() {
        return Stream.of(
            createValidCallStream().map(x -> x.put("params", new OrderedParameters())),
            createValidCallStream().map(x -> x.put("params", OrderedParameters.from(List.of("bar"))))
            ).flatMap(i -> i);
    }

    @DisplayName("OrderedParametersRequest(final JSONObject remoteCall) constructor")
    @ParameterizedTest
    @MethodSource("createValidOrderedParametersCallStream")
    void constructor8Test(final JSONObject validCall) throws InvalidRequestError {
        // OrderedParametersRequest(final JSONObject remoteCall) throws InvalidRequestError
        final OrderedParametersRequest request = new OrderedParametersRequest(validCall);

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertTrue(obj.similar(validCall));
    }

    /*
        NamedParametersRequest Tests
    */

    static Stream<NamedParameters> createValidNamedParametersStream() {
        return Stream.of(
                NamedParameters.from(Map.of("abc", 123)),
                NamedParameters.from(Map.of()),
                new NamedParameters(),
                (NamedParameters)null
            );
    }

    @DisplayName("NamedParametersRequest(final String methodName, final NamedParameters parameters) constructor")
    @ParameterizedTest
    @MethodSource("createValidNamedParametersStream")
    void constructor9Test(final NamedParameters parameters) {
        // NamedParametersRequest(final String methodName, final OrderedParameters parameters)
        final String methodName = "foo";
        final NamedParametersRequest request = new NamedParametersRequest(methodName, parameters);

        assertEquals(methodName, request.getMethod());
        assertEquals(null, request.getId());
        assertTrue(request.isNotification());

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    @DisplayName("NamedParametersRequest(final Object id, final String methodName, final NamedParameters parameters) constructor")
    @ParameterizedTest
    @MethodSource("createValidNamedParametersStream")
    void constructor10Test(final NamedParameters parameters) {
        // NamedParametersRequest(final Object id, final String methodName, final NamedParameters parameters)
        final String methodName = "foo";
        final String id = "uuid";
        final NamedParametersRequest request = new NamedParametersRequest(id, methodName, parameters);

        assertEquals(methodName, request.getMethod());
        assertEquals(id, request.getId());
        assertFalse(request.isNotification());

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertEquals("2.0", obj.getString("jsonrpc"));
    }

    private static Stream<NamedParameters> createValidNamedParametersJSONValueStream() {
        return Stream.of(
            new NamedParameters(),
            NamedParameters.from(Map.of("abc", 123)),
            NamedParameters.from(Map.of())
        );
    }

    private static Stream<Object> createdInvalidNamedParametersJSONValueStream() {
        return Stream.of(
            "string",
            1,
            true,
            false,
            JSONObject.NULL,
            new JSONArray()
        );
    }

    static Stream<JSONObject> createInvalidNamedParametersCallStream() {
        return Stream.of(
            createInvalidCallStream().map(
                c -> createValidNamedParametersJSONValueStream().map(p -> c.put("params", p))
            ).flatMap(i -> i),
            createValidCallStream().map(
                c -> createdInvalidNamedParametersJSONValueStream().map(p -> c.put("params", p))
            ).flatMap(i -> i)
        ).flatMap(i -> i);
    }

    @DisplayName("NamedParametersRequest(final JSONObject remoteCall) constructor throws InvalidRequestError")
    @ParameterizedTest
    @MethodSource("createInvalidNamedParametersCallStream")
    void constructor11Test(final JSONObject invalidCall) {
        // public NamedParametersRequest(final JSONObject remoteCall) throws InvalidRequestError
        assertThrows(InvalidRequestError.class, () -> {
            new NamedParametersRequest(invalidCall);
        });
    }

    static Stream<JSONObject> createValidNamedParametersCallStream() {
        return createValidCallStream().map(
            c -> createValidNamedParametersJSONValueStream().map(p -> c.put("params", p))
        ).flatMap(i -> i);
    }

    @DisplayName("NamedParametersRequest(final JSONObject remoteCall) constructor")
    @ParameterizedTest
    @MethodSource("createValidNamedParametersCallStream")
    void constructor12Test(final JSONObject validCall) throws InvalidRequestError {
        // public NamedParametersRequest(final JSONObject remoteCall) throws InvalidRequestError
        final NamedParametersRequest request = new NamedParametersRequest(validCall);

        final JSONObject obj = new JSONObject(request.toJSON());
        assertTrue(obj.similar(request.toJSONObject()));
        assertTrue(obj.similar(validCall));
    }

}