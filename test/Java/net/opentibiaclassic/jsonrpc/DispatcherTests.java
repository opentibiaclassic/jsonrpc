package test.Java.net.opentibiaclassic.jsonrpc;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.function.Executable;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigInteger;

import java.util.stream.Stream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.concurrent.Callable;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.ValidRequestProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.AmbiguousMethodExceptionProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.MissingParametersProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.WrongParameterTypesProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.requestbuilder.InvalidMethodNameProvider;
import test.Java.net.opentibiaclassic.jsonrpc.dispatcher.TestRPCServer;

import static test.net.opentibiaclassic.Util.Math.InclusiveRange;

class DispatcherTests {

    @BeforeAll
    static void initAll() {
    }

    @BeforeEach
    void init() {

    }

    /*
        5 Response object

        When a rpc call is made, the Server MUST reply with a Response, except for in the case of Notifications. The Response is expressed as a single JSON Object, with the following members:

        jsonrpc
            A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".

        result
            This member is REQUIRED on success.
            This member MUST NOT exist if there was an error invoking the method.
            The value of this member is determined by the method invoked on the Server.

        error
            This member is REQUIRED on error.
            This member MUST NOT exist if there was no error triggered during invocation.
            The value for this member MUST be an Object as defined in section 5.1.

        id
            This member is REQUIRED.
            It MUST be the same as the value of the id member in the Request Object.
            If there was an error in detecting the id in the Request object (e.g. Parse error/Invalid Request), it MUST be Null.

        Either the result member or error member MUST be included, but both members MUST NOT be included.

        5.1 Error object

        When a rpc call encounters an error, the Response Object MUST contain the error member with a value that is a Object with the following members:

        code
            A Number that indicates the error type that occurred.
            This MUST be an integer.
        message
            A String providing a short description of the error.
            The message SHOULD be limited to a concise single sentence.
        data
            A Primitive or Structured value that contains additional information about the error.
            This may be omitted.
            The value of this member is defined by the Server (e.g. detailed error information, nested errors etc.).

        The error codes from and including -32768 to -32000 are reserved for pre-defined errors. Any code within this range, but not defined explicitly below is reserved for future use. The error codes are nearly the same as those suggested for XML-RPC at the following url: http://xmlrpc-epi.sourceforge.net/specs/rfc.fault_codes.php
        code    message     meaning
        -32700  Parse error     Invalid JSON was received by the server.
        An error occurred on the server while parsing the JSON text.
        -32600  Invalid Request     The JSON sent is not a valid Request object.
        -32601  Method not found    The method does not exist / is not available.
        -32602  Invalid params  Invalid method parameter(s).
        -32603  Internal error  Internal JSON-RPC error.
        -32000 to -32099    Server error    Reserved for implementation-defined server-errors.

        The remainder of the space is available for application defined errors.
    */

    private static final Map<String, Integer> ERROR_CODES = Map.of(
        "parse_error", -32700,
        "invalid_request", -32600,
        "method_not_found", -32601,
        "invalid_params", -32602,
        "internal_error", -32603,
        "server_min", -32099,
        "server_max", -32000
    ); 

    private static void assertServerErrorCode(final int code) {
        assertTrue(new InclusiveRange<Integer>(
                ERROR_CODES.get("server_min"),
                ERROR_CODES.get("server_max")
            ).contains(code)
        );
    }

    private static void assertParseErrorCode(final int code) {
        assertEquals(ERROR_CODES.get("parse_error"), code);
    }

    private static void assertJSONRPCError(final JSONObject error) {
        final int code = error.getInt("code");
        final String message = error.getString("message");
    }

    private static void assertJSONRPCServerError(final JSONObject error) {
        assertJSONRPCError(error);
        assertServerErrorCode(error.getInt("code"));
    }

    private static void assertJSONRPCParseError(final JSONObject error) {
        assertJSONRPCError(error);
        assertParseErrorCode(error.getInt("code"));
    }

    private static void assertJSONRPCResponse(final JSONObject response) {
        assertEquals("2.0", response.getString("jsonrpc"));
        assertTrue(response.has("id"));
        assertTrue(response.has("error") ^ response.has("result"));
    }

    private static void assertJSONRPCErrorResponse(final JSONObject response) {
        assertJSONRPCResponse(response);
        assertTrue(response.has("error"));
    }

    private static void assertJSONRPCServerErrorResponse(final JSONObject response) {
        assertJSONRPCErrorResponse(response);
        assertJSONRPCServerError(response.getJSONObject("error"));
    }

    private static void assertJSONRPCParseErrorResponse(final JSONObject response) {
        assertJSONRPCErrorResponse(response);
        assertEquals(JSONObject.NULL, response.get("id"));
        assertJSONRPCParseError(response.getJSONObject("error"));
    }

    @ParameterizedTest
    @NullSource
    void nullTest(final String request) {
        final TestRPCServer server = new TestRPCServer();
        final JSONObject response = new JSONObject(server.dispatch(request));

        assertJSONRPCServerErrorResponse(response);
        assertEquals(JSONObject.NULL, response.get("id"));
    }

    @ParameterizedTest
    @EmptySource
    void emptyTest(final String request) {
        final TestRPCServer server = new TestRPCServer();
        assertNull(server.dispatch(request));
    }

    // TODO add test that checks parsed string message (jsonrpc "2.0" != Const.jsonrpcversion)

    // TODO invalid requests, NOT parse errors "[{},]", "[{}],"

    @ParameterizedTest
    @ValueSource(strings = { "{", "[", "[{]", "[{},{]", "null"})
    void malformedTest(final String request) {
        final TestRPCServer server = new TestRPCServer();
        final JSONObject response = new JSONObject(server.dispatch(request));

        assertJSONRPCParseErrorResponse(response);
    }

    // TODO test [{valid call}, null, {valid call}]
    // TODO test [{valid call}, 0, {valid call}]
    // TODO test [{valid call}, true, {valid call}]
    // TODO test [{valid call}, "", {valid call}]

}