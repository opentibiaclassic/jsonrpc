#!/bin/bash
set -x
rm -rf build/Java/*
javac -d build/Java --module-path lib/Java --module-source-path src/Java --module net.opentibiaclassic.jsonrpc
javac -d build/Java --module-path build/Java:lib/Java --add-modules net.opentibiaclassic.jsonrpc,junit.platform.console.standalone test/Java/net/opentibiaclassic/jsonrpc/*.java