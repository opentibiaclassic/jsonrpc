#!/bin/bash
. constants.env
jarfile="dist/net.opentibiaclassic.jsonrpc_${release}.jar"
set -x

rm -rf $jarfile
jar cf $jarfile -C build/Java/net.opentibiaclassic.jsonrpc .